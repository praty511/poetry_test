from poetry_test import __version__
from poetry_test import calculator

def test_version():
    assert __version__ == '0.1.0'

def test_addition():
    assert 4 == calculator.add(2, 2) 

def test_subtraction():
    assert 5 == calculator.subtract(10,5)

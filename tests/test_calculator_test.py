from poetry_test.calculator import subtract, add
import pytest

def test_addition():
        assert 5 == add(3, 2)

def test_subtraction():
        assert 0 == subtract(2, 2)